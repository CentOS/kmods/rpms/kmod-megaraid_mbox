%global pkg megaraid_mbox

%global kernel 5.14.0-362.13.1.el9_3
%global baserelease 2

%global debug_package %{nil}

%global __spec_install_post \
  %{?__debug_package:%{__debug_install_post}} \
  %{__arch_install_post} \
  %{__os_install_post} \
  %{__mod_compress_install_post}

%global __mod_compress_install_post %{nil}


Name:             kmod-%{pkg}
Epoch:            1
Version:          %(echo %{kernel} | sed 's/-/~/g; s/\.el.*$//g')
Release:          %{baserelease}%{?dist}
Summary:          LSI Logic MegaRAID (%{pkg}) driver

License:          GPLv2 and GPLv2+
URL:              https://www.kernel.org/

Patch0:           source-git.patch

ExclusiveArch:    x86_64 aarch64 ppc64le

BuildRequires:    elfutils-libelf-devel
BuildRequires:    gcc
BuildRequires:    kernel-rpm-macros
BuildRequires:    kmod
BuildRequires:    make
BuildRequires:    redhat-rpm-config

BuildRequires:    kernel-abi-stablelists = %{kernel}
BuildRequires:    kernel-devel-uname-r = %{kernel}.%{_arch}

Requires:         kernel-uname-r >= %{kernel}.%{_arch}

Provides:         installonlypkg(kernel-module)
Provides:         kernel-modules >= %{kernel}.%{_arch}

Requires(posttrans): %{_sbindir}/depmod
Requires(postun):    %{_sbindir}/depmod

Requires(posttrans): %{_sbindir}/weak-modules
Requires(postun):    %{_sbindir}/weak-modules

Requires(posttrans): %{_bindir}/sort
Requires(postun):    %{_bindir}/sort


Provides:         kmod-megaraid_mm = %{?epoch:%{epoch}:}%{version}-%{release}

%if %{epoch}
Obsoletes:        kmod-%{pkg} < %{epoch}:
%endif
Obsoletes:        kmod-%{pkg} = %{?epoch:%{epoch}:}%{version}


%description
This package provides the LSI Logic MegaRAID (%{pkg}) driver including the
required LSI Logic Management Module (megaraid_mm) driver. Supported devices:

- 0x1000:0407:1000:0530: LSI MegaRAID SCSI 320-0X
- 0x1000:0407:1000:0531: LSI MegaRAID SCSI 320-4X
- 0x1000:0407:1000:0532: LSI MegaRAID SCSI 320-2X
- 0x1000:0407:1028:0531: Dell PERC4/QC
- 0x1000:0407:8086:0530: Intel RAID Controller SRCZCRX
- 0x1000:0407:8086:0532: Intel RAID Controller SRCU42X
- 0x1000:0408:1000:0001: LSI MegaRAID SCSI 320-1E
- 0x1000:0408:1000:0002: LSI MegaRAID SCSI 320-2E
- 0x1000:0408:1025:004D: ACER MegaRAID ROMB-2E
- 0x1000:0408:1028:0001: Dell PERC 4e/SC
- 0x1000:0408:1028:0002: Dell PERC 4e/DC
- 0x1000:0408:1033:8287: NEC MegaRAID PCI Express ROMB
- 0x1000:0408:1734:1065: FSC MegaRAID PCI Express ROMB
- 0x1000:0408:8086:0002: Intel RAID Controller SRCU42E
- 0x1000:0408:8086:3431: Intel RAID Controller SROMBU42E
- 0x1000:0408:8086:3499: Intel RAID Controller SROMBU42E
- 0x1000:0409:1000:3004: LSI MegaRAID SATA 300-4X
- 0x1000:0409:1000:3008: LSI MegaRAID SATA 300-8X
- 0x1000:0409:8086:3008: Intel RAID Controller SRCS28X
- 0x1000:1960:1000:0518: LSI MegaRAID SCSI 320-2
- 0x1000:1960:1000:0520: LSI MegaRAID SCSI 320-1
- 0x1000:1960:1000:0523: LSI MegaRAID SATA 150-6
- 0x1000:1960:1000:4523: LSI MegaRAID SATA 150-4
- 0x1000:1960:1000:A520: LSI MegaRAID SCSI 320-0
- 0x1000:1960:1028:0518: Dell PERC4/DC
- 0x1000:1960:1028:0520: Dell PERC4/SC
- 0x1000:1960:8086:0520: Intel RAID Controller SRCU51L
- 0x1000:1960:8086:0523: Intel RAID Controller SRCS16
- 0x101E:1960:1028:0471: Dell PERC3/QC
- 0x101E:1960:1028:0475: Dell PERC3/SC
- 0x101E:1960:1028:0493: Dell PERC3/DC
- 0x1028:000E:1028:0123: Dell PERC3/Di
- 0x1028:000F:1028:014A: Dell PERC4/Di
- 0x1028:0013:1028:016C: Dell PERC 4e/Si
- 0x1028:0013:1028:016D: Dell PERC 4e/Di
- 0x1028:0013:1028:016E: Dell PERC 4e/Di
- 0x1028:0013:1028:016F: Dell PERC 4e/Di
- 0x1028:0013:1028:0170: Dell PERC 4e/Di


%prep
%autosetup -p1 -c -T


%build
pushd src
%{__make} -C /usr/src/kernels/%{kernel}.%{_arch} %{?_smp_mflags} M=$PWD modules
popd


%install
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/megaraid src/%{pkg}.ko src/megaraid_mm.ko

# Make .ko objects temporarily executable for automatic stripping
find %{buildroot}/lib/modules -type f -name \*.ko -exec chmod u+x \{\} \+


%clean
%{__rm} -rf %{buildroot}


%triggerin -- %{name}
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/megaraid/%{pkg}.ko" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/megaraid/megaraid_mm.ko" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules


%triggerun -- %{name}
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/megaraid/%{pkg}.ko" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/megaraid/megaraid_mm.ko" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules


%preun
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
rpm -ql kmod-%{pkg}-%{?epoch:%{epoch}:}%{version}-%{release}.%{_arch} | grep '/lib/modules/%{kernel}.%{_arch}/.*\.ko$' >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove


%postun
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules ]
then
    while read -r MODULE
    do
        if [ -f "$MODULE" ]
        then
            printf '%s\n' "$MODULE" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
        fi
    done < %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove
    if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
    then
        printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --remove-modules --no-initramfs
    else
        printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --remove-modules
    fi
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
    printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --add-modules
fi
if [ -d %{_localstatedir}/lib/rpm-state/sig-kmods ]
then
    rmdir --ignore-fail-on-non-empty %{_localstatedir}/lib/rpm-state/sig-kmods
fi


%posttrans
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules ]
then
    while read -r MODULE
    do
        if [ -f "$MODULE" ]
        then
            printf '%s\n' "$MODULE" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
        fi
    done < %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
    printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --add-modules
fi
if [ -d %{_localstatedir}/lib/rpm-state/sig-kmods ]
then
    rmdir --ignore-fail-on-non-empty %{_localstatedir}/lib/rpm-state/sig-kmods
fi


%files
%defattr(644,root,root,755)
/lib/modules/%{kernel}.%{_arch}
%license licenses


%changelog
* Fri Jul 12 2024 Peter Georg <peter.georg@physik.uni-regensburg.de> - 1:5.14.0~362.13.1-2
- Do not compress kernel modules

* Fri Feb 16 2024 Kmods SIG <sig-kmods@centosproject.org> - 1:5.14.0~362.13.1-1
- kABI tracking kmod package (kernel >= 5.14.0-362.13.1.el9_3)
